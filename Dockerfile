FROM mono:latest

ARG downloadUrl
ARG version

LABEL maintainer="PiekJ<info@jospiek.nl>" \
      description="Running DocFx $version (with mono)"

RUN apt-get update && apt-get install unzip wget git -y
RUN wget -q -P /tmp $downloadUrl && \
    mkdir -p /opt/docfx && \
    unzip /tmp/docfx.zip -d /opt/docfx && \
    echo '#!/bin/bash\nmono /opt/docfx/docfx.exe $@' > /usr/bin/docfx && \
    chmod +x /usr/bin/docfx && \
    rm -f /tmp/*